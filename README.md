# jicl

#### Table of Contents

1. [Overview](#overview)
2. [Module Description - What the module does and why it is useful](#module-description)
3. [Setup - The basics of getting started with jicl](#setup)
    * [What jicl affects](#what-jicl-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with jicl](#beginning-with-jicl)
4. [Usage - Configuration options and additional functionality](#usage)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Overview

Install and configure the Java InterCom Layer (JICL), used by various
detectors in ALICE.

## Module Description

...

## Setup

### What jicl affects

* installs the JICL package
* creates configuration files
* ensures that the JICL service is running

### Beginning with jicl

The very basic steps needed for a user to get the module up and running.

If your most recent release breaks compatibility or requires particular steps
for upgrading, you may wish to include an additional section here: Upgrading
(For an example, see http://forge.puppetlabs.com/puppetlabs/firewall).

## Usage

Put the classes, types, and resources for customizing, configuring, and doing
the fancy stuff with your module here.

## Reference


## Limitations

Tested only on Scientific Linux / CentOS.

## Development

Check out gitlab.cern.ch


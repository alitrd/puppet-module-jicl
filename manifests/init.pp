# Class: jicl
#
# Install and configure the Java InterCom Layer (JICL)
#
# Parameters:
#   $first_parameter:
#       description
#
# Actions:
#   - 
#
# Requires:
#   - 
#
# Sample Usage:
#
class jicl (
  $ensure = 'running',
  $wingdb_host,
  $wingdb_user,
  $wingdb_pass
)

{
  

  package {

      [ 'jicl', 'libjdim1', 'java-1.7.0-openjdk' ] :

        ensure => $ensure ? {
          'running'     => 'installed',
          'configured'  => 'installed',
          'absent'      => 'absent',
        }
        
  }

  file_line { 'jicl-chkconfig':
    path  => '/etc/init.d/jicl',
    line  => '# chkconfig: 2345 90 04',
    match => '^# chkconfig:',

    subscribe => Package['jicl'],
    notify => Service['jicl'],
  }

  
  if ($ensure == 'running' or $ensure == 'configured') {

    class { 'jicl::config':
      wingdb_host => $wingdb_host,
      wingdb_user => $wingdb_user,
      wingdb_pass => $wingdb_pass,

      require => Package['jicl'],
      notify => Service['jicl'],
    }

  }
  else {

    file {
      [ '/etc/default/jicl',
        '/etc/jicl/init.conf',
        '/etc/jicl/Database.txt',
        '/etc/jicl/Property.txt']:
          
          ensure => absent,
    }
  }
  
  
  service {
    'jicl':
      ensure => $ensure ? {
        'running' => 'running',
        default   => 'stopped'
      },
      
      hasstatus => true,
      hasrestart => true,
  }
  

}



# Local Variables:
#   mode: puppet
#     puppet-indent-level: 4
#     indent-tabs-mode: nil
# End:

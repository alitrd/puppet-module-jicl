
class jicl::config (
  $wingdb_host,
  $wingdb_user,
  $wingdb_pass
)
{
  File {
    require => Package['jicl'],
    #notify => Service['jicl'],
    owner => 'root', group => 'root', mode => '0644',
  }
  
  file {
    '/etc/jicl':
      ensure => directory,
      before => File[ '/etc/jicl/Database.txt',
                      '/etc/jicl/Property.txt',
                      '/etc/jicl/init.conf'];
                      
    '/etc/jicl/Database.txt':
      ensure => file,
      content => template('jicl/Database.txt.erb');

    '/etc/jicl/Property.txt':
      ensure => file,
      content => template('jicl/Property.txt.erb');

    '/etc/jicl/init.conf':
      ensure => file,
      content => template('jicl/init.conf.erb');

    '/etc/default/jicl':
      ensure => file,
      content => template('jicl/default.erb');
  }
}
